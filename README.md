Docker Compose for single instance Zookeeper, Kafka, and Cassandra
=================================

This will install docker images for Zookerper, Kafka and Cassandra

# Installation

Download and install docker. Git clone this into a directoy and run:

```
docker-compose up
```

## Clear data cache

Run `reset_all_data.sh` this will delete all information in your zookeeper, kafka, and cassandra data folders

## Access Cassandra

Install CQLSH and run:

`cqlsh --cqlversion="3.4.4"`


## Ports

All services run on your localhost and uses standard ports

Kafka: 9092
Cassandra: 9042
Zookeeper: 2181
