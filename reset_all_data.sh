echo "Resetting al data by deleting all data directories for zookeeper, kafka, cassandra"
sudo rm -rf ./zk-single-kafka-single/zoo1/data/*
sudo rm -rf ./zk-single-kafka-single/zoo1/datalog/*
sudo rm -rf ./zk-single-kafka-single/kafka1/data/*
sudo rm -rf ./data/cassandra/*
